var gulp = require('gulp');
var plumber = require('gulp-plumber');
var imageMin = require('gulp-imagemin');
var cache = require('gulp-cache');
var notify = require('gulp-notify');
gulp.task('css',function(){
    gulp.src(['src/css/**/*.css'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(gulp.dest('gulp/css'))
        .pipe(notify('css task finished'))
});
gulp.task('js',function(){
    gulp.src(['src/js/**/*.js'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(gulp.dest('gulp/js'))
          .pipe(notify('js task finished'))
});
gulp.task('html',function(){
    gulp.src(['src//**/*.html'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(gulp.dest('gulp/'))
        .pipe(notify('html task finished'))
});
gulp.task('image',function(){
    gulp.src(['src/images/**/*'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(cache(imageMin()))
        .pipe(gulp.dest('gulp/images'))
        .pipe(notify('image task finished'))
});
gulp.task('default',function(){
    gulp.watch('src/js/**/*.js',['js']);
    gulp.watch('src/css/**/*.css',['css']);
    gulp.watch('src//**/*.html',['html']);
    gulp.watch('src/images/**/*',['image']);
});
